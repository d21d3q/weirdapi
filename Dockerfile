FROM python:3.7-alpine

ENV PYTHONUNBUFFERED=1 \
  POETRY_VERSION=0.12.12

RUN mkdir /app
WORKDIR /app

RUN pip install "poetry==$POETRY_VERSION"

COPY ./pyproject.toml ./poetry.lock /app/

RUN poetry config settings.virtualenvs.create false \
  && poetry install --no-dev --no-interaction

COPY . /app/

EXPOSE 8080

CMD ["python", "-m", "weirdapi"]
