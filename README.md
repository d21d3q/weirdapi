# About
This repository contains example project which implements weirdtext text encoding,
and exposes rest api.

# Usage
## manual 
Install [poetry](https://pypi.org/project/poetry/), clone repository:

```bash
git clone https://gitlab.com/d21d3q/weirdapi/
cd weirdapi
```
setup and run:
```bash
# optional virtualenv
virutalenv venv
source venv/bin/activate

poetry install
python -m weirdapi
```
or use docker instead:
```bash
docker-compose up --build
```

App will expose endpoints `/api/v1/encode`, `/api/v1/decode` for text encoding/decoding
and `/api/doc` which server swagger documentation.

# Deploying

Deploy with automatic updating (CD) using [traefik](https://traefik.io/)
and [watchover](https://github.com/containrrr/watchtower) by creating `docker-compose.yml`:

```yml
version: "3"
services:
  weirdapi:
    container_name: weirdapi
    image: registry.gitlab.com/d21d3q/weirdapi:latest
    labels:
      - "traefik.docker.network=web"
      - "traefik.enabled=true"
      - "traefik.frontend.rule=Host:weirdapi.example.com"

  watchtower:
    image: containrrr/watchtower
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock

    command: weirdapi
```
> Note double `$` in `ReplacePathRegex` (yaml requires to escape it)

# Testing
Test and lint by running
```bash
pytest --flake8
# or ptw for reloading
ptw -c -- --flake8
```
