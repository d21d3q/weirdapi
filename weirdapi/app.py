"""This module provices main app for weirdapi"""
from aiohttp.web import Application, run_app
from aiohttp_swagger import setup_swagger

from .handlers import routes


def make_app():
    """
    application factoy

    :returns: weird Application ready to run
    """
    app = Application()
    api = Application()

    api.add_routes(routes)
    app.add_subapp('/api/v1', api)
    swagger_description = """
    This api provides encoder and decored for weirdtext
    """
    setup_swagger(app,
                  # swagger_url='/doc',
                  title='Weirdtext api',
                  description=swagger_description)
    return app


def main():
    """
    Main application running server
    """
    app = make_app()
    run_app(app)


if __name__ == '__main__':
    main()
