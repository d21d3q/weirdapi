from aiohttp.web import RouteTableDef, json_response

from .weirdtext import encode, decode
from .weirdtext.decoder import NotWeirdTextError


routes = RouteTableDef()


@routes.post('/encode')
async def encode_handler(request):
    """
    weirdtext encode handler
    ---
    description: Weirdtext encode
    consumes:
      - "application/json"
    produces:
      - "application/json"
    parameters:
    - name: body
      in: body
      required: true
      description: Text to be encoded
      schema:
        type: object
        properties:
          text:
            type: string
            example: >
              This is a long looong test sentence,

              with some big (biiiig) words!

    responses:
      200:
        description: Successfull encoding
      400:
        description: Bad input data
    """
    try:
        data = await request.json()
    except ValueError:
        return json_response({'message': 'Unable to decode json body'},
                             status=400)

    if 'text' not in data:
        return json_response({'message': 'Text not found'},
                             status=400)

    encoded = encode(data['text'])
    return json_response({'encoded': encoded})


@routes.post('/decode')
async def decode_handler(request):
    """
    weirdtext decoder handler
    ---
    description: Weirdtext decoder
    consumes:
      - "application/json"
    produces:
      - "application/json"
    parameters:
    - name: body
      in: body
      required: true
      description: Text to be decoded
      schema:
        type: object
        properties:
          text:
            type: string
            example: >

              -weird-

              Tihs is a lnog loonog tset sntceene,

              wtih smoe big (biiiiig) wdros!

              -weird-

              long looong sentence some test This with words


    produces:
      - "application/json"
    responses:
      200:
        description: Successfull encoding
      400:
        description: Bad input data
    """
    try:
        data = await request.json()
    except ValueError:
        return json_response({'message': 'Unable to decode json body'},
                             status=400)

    if 'text' not in data:
        return json_response({'message': 'Text not found'},
                             status=400)

    try:
        decoded = decode(data['text'])
        return json_response({'decoded': decoded})

    except NotWeirdTextError:
        return json_response({'message': 'Text not in weirdtext format'},
                             status=400)
