"""This moule provides weirdtext encoder and ecoder"""
from .encoder import encode
from .decoder import decode


__all__ = ['encode', 'decode']
