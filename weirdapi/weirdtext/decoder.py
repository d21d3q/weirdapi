"""This moule implements weirdtext decoder"""
import re
from .defs import MAGIC_PATTERN


class NotWeirdTextError(Exception):
    """Error to be raised when decoded text
    does not have proper format"""


def parse_encoded(encoded):
    """
    This method parses encoded string by looking for magic tokens
    and splitting it to body and list of words.

    :param encode: encode text to be parsed
    :returns: tuple containing body and list of words
    :raises NotWeirdTextError: when given string does not looks like encoded
        text
    """
    pattern = (r'{MAGIC}(?P<body>[\s\S]*){MAGIC}(?P<words>[\s\w]*)'
               .format(MAGIC=MAGIC_PATTERN))

    match = re.match(pattern, encoded)

    if not match:
        raise NotWeirdTextError()

    return (match.group('body'), match.group('words').split())


def decode(text):
    """
    Decodes weird encoded text.

    :param text: weird encoded text
    :returns: decoded text
    """
    body, words = parse_encoded(text)

    if not words:
        return body

    occurences = {}
    indexes = set()

    # count all occurences of words in list
    for word in words:
        if len(word) > 3:
            if word in occurences:
                occurences[word]['count'] += 1
            else:
                occurences[word] = {'count': 1, 'indexes': set()}

    for word in occurences:
        counter = 0
        # find all similar words in body
        pattern = (r'\b{first}[{internal}]{{{count}}}{last}\b'
                   .format(first=word[0],
                           internal=word[1:-1],
                           count=len(word[1:-1]),
                           last=word[-1]))

        for match in re.finditer(pattern, body):
            if match.start() not in indexes:
                # if we havent seen word on this position
                # add it to seen list otherwise this mach
                # was matched previously by other same word
                # or word with same 'internal'
                indexes.add(match.start())
                occurences[word]['indexes'].add(match.start())
                counter += 1
                # if we reached number of substituted words, then
                # lets break. remaining occurences should be matched by
                # similar words
                if counter >= occurences[word]['count']:
                    break

    decoded = body
    for word, meta in occurences.items():
        for index in meta['indexes']:
            decoded = decoded[:index] + word + decoded[index + len(word):]

    return decoded
