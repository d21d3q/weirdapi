"""This moule implements weirdtext decoder"""
import re
import random

from .defs import MAGIC


def shuffle_word(word):
    """Shuffles internal letters in word.

    :param word: input word
    :returns: shuffeled word
    """
    to_shuffle = list(word[1:-1])
    # shuffling word coul be done in loop until it will be
    # different than input. For words 4 characters long
    # there are 50% chances, that they will be the same
    random.shuffle(to_shuffle)
    return word[0] + ''.join(to_shuffle) + word[-1]


def encode(text):
    """
    Encodes text with 'weirdtext algorithm'

    :param text: text to be endoded
    :returns: endoded text
    """
    encoded = ''
    end = 0
    shuffled = []
    # exclude words that have less than 4 characters or contain
    # same characters inside
    tokenize = re.compile(r'(?!\b\w(\w)\1+\w\b|\b\w{1,3}\b)(\b\w+\b)')

    for match in tokenize.finditer(text):
        shuffled.append(match.group())
        if match.start() > end:
            encoded += text[end:match.start()]

        encoded += shuffle_word(match.group())

        end = match.end()

    if end < len(text):
        encoded += text[end:]

    return MAGIC + encoded + MAGIC + ' '.join(sorted(shuffled))
