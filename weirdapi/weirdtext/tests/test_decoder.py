import pytest

from ..decoder import parse_encoded, NotWeirdTextError, decode


def test_parse_encoded_valid_input():
    encoded = """
-weird-
Tihs is a lnog loonog tset sntceene,
wtih smoe big (biiiiig) wdros!
-weird-
long looong sentence some test This with words"""

    body, words = parse_encoded(encoded)

    assert body == """Tihs is a lnog loonog tset sntceene,
wtih smoe big (biiiiig) wdros!"""

    assert words == ['long', 'looong', 'sentence', 'some', 'test', 'This',
                     'with', 'words']


INVALID_INPUTS = [
    # no magic token
    """some text
weird really text""",

    # no new line on beginning of string
    """-weird-
Tihs is a lnog loonog tset sntceene,
wtih smoe big (biiiiig) wdros!
-weird-long looong sentence some test This with words""",

    # only one magic token
    """
-weird-
Tihs is a lnog loonog tset sntceene,
wtih smoe big (biiiiig) wdros!
long looong sentence some test This with words""",

]


@pytest.mark.parametrize('encoded', INVALID_INPUTS)
def test_parse_encoded_invali_input(encoded):
    with pytest.raises(NotWeirdTextError):
        parse_encoded(encoded)


def test_decode_with_no_words():
    encoded = """
-weird-
Tihs is a lnog loonog tset sntceene,
wtih smoe big (biiiiig) wdros!
-weird-
"""
    decoded = decode(encoded)
    assert decoded == """Tihs is a lnog loonog tset sntceene,
wtih smoe big (biiiiig) wdros!"""


TEXT_PARIS = [
    ('\n-weird-\nsmoe bsaic tset\n-weird-\nbasic some test',
     'some basic test'),
    ('\n-weird-\nsmoe bsaic smoe tset\n-weird-\nbasic some some test',
     'some basic some test'),
    ('\n-weird-\nsmoe legnor lonegr smoe tset'
     '\n-weird-\nbasic longer lnoger smoe some test',
     'smoe longer lnoger some test'),
    ('\n-weird-\nsmoe legnor lgenor lonegr smoe tset'
     '\n-weird-\nbasic longer longer lnoger some some test',
     'some longer longer lnoger some test'),
        ]


@pytest.mark.parametrize('encoded, expected', TEXT_PARIS)
def test_decode(encoded, expected):
    decoded = decode(encoded)
    assert decoded == expected
