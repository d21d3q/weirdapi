from ..encoder import encode


def test_encode():
    text = """lorem ipsum dolor
domine lol; kotine"""

    output = encode(text)
    assert text != output
